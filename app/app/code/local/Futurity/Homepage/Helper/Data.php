<?php
class Futurity_Homepage_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected function getCategoryCollection(){
    $collection = Mage::getModel('catalog/category')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToSort('position');
        return $collection;
    }

    public function PrintSubCategoriesByKey($key){
        $categories = $this->getCategoryCollection();
        $output = '';
        foreach ($categories as $category):
            if ($category->getLevel() == 2 && $category->getUrlKey() == $key):
                $output .= sprintf('<a href="%s">%s</a>',$category->getUrl(),$category->getName());
                if($category->getChildren() != ''):
                    $subcatArray = explode(',', $category->getChildren());
                    $output .= '<ul>';
                        foreach($subcatArray as $subcategory):
                            $_subCat = Mage::getModel('catalog/category')->load($subcategory);
                            $output .= sprintf('<li><a href="%s">%s</a></li>',$_subCat->getUrl(),$_subCat->getName());
                        endforeach;
                    $output .= '</ul>';
                endif;
            endif;
        endforeach;
        return $output;
    }
}