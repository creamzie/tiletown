<?php
class Collorce_Base_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
	public function addNewCategoriesFromList( array $categories, $parentCategory, $storeId )
	{
		foreach ( $categories as $category ) {
			$newId = Mage::getModel('catalog/category')
				->setStoreId(0)
				->setPath($parentCategory->getPath())
				->setParentId($parentCategory->getId())
				->setName($category['name'])
				->setUrlKey($category['urlKey'])
				->setIsActive(true)
				->setIncludeInMenu(true)
				->save()
				->getId();

			$currentCategory = Mage::getModel('catalog/category')->load($newId);
			$currentCategory->setStoreId($storeId)
				->setName($category['title'])
				->save();

			if ( isset( $category['subCategories'] )) {
				$this->addNewCategoriesFromList( $category['subCategories'], $currentCategory, $storeId );
			}
		}
	}
}