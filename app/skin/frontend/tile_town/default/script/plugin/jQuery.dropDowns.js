jQuery(function($){
	function setDDItem ( item ) {
		// $(this).parent().prepend($(this));
		var container = $(item).parent().parent();
		container.find( '.active' ).removeClass( 'active' ); // Remove previous active link
		container.find( '.title' ).text( $(item).text() ); // Set title
		$(item).addClass( 'active' ); // Set new active link

		// Change selected item in select box
		var select = container.next();
		$('[selected]', select).removeAttr( 'selected' );
		$('option', select).eq( $(item).prevAll().length ).attr( {selected: 'selected'} );
	}

	var searchDD = $('select.dropDown'), // find drop downs
		ddId, ddElm, ddList, ddCurrent, ddElms,
		ddClass = 'styledDropDown';
	$.each( searchDD, function( i, elm ) {
		ddId = 'styled-' + $(elm).attr( 'id' ); // Make the new id for the styled drop down
		ddList = $('<ul></ul>'); // Create the list for the styled drop down

		ddCurrent = 0; // Set current item to first in the list
		$('option', elm).each( function( i, elm ) { // Switch options of drop down and create list items for styled drop down
			ddList.append( $('<li></li>') // Create the new list item
				.text( $(elm).text() )
			);
			if ( 'selected' == $(elm).attr('selected') )
				ddCurrent = i;
		});

		ddElm = $('<span></span>') // Create the new drop down wrapper
			.addClass( ddClass )
			.addClass( 'ddHidden' )
			.attr( 'id', ddId )
		.prepend( $('<div></div>').addClass( 'title' ) ) // Add title block
		.append( ddList );

		setDDItem( $('li', ddElm).eq( ddCurrent ) ); // Set current item

		$(elm).css( 'display', 'none' ) // Paste the new drop down
			.before( ddElm );
	});

	ddElms = $('.' + ddClass);

	$('.title', ddElms).click(function(e){ // Show or close drop down list when has click on title
		var dd = $(this).parent();
		if ( dd.hasClass( 'ddHidden' ) ) {
			ddElms.addClass( 'ddHidden' );
			dd.removeClass( 'ddHidden' );
		} else
			dd.addClass( 'ddHidden' );
	});

	$('li', ddElms).click(function(e){ // Bind activation
		setDDItem( this ); // Change active item to pushed
		$(this).parent().parent().addClass( 'ddHidden' ); // Hide drop down list
	});

	$('body').click(function(e){ // Hide drop down lists when focus is out
		var clicked = $(e.target).parents( '.styledDropDown' );
		if ( ! clicked.length )
			$('.styledDropDown').addClass( 'ddHidden' );
	});
});