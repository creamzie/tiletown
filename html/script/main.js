jQuery(function($){
	/* Main Navigation
	-----------------------------------*/
	$('#navItemCategory, #navItemCategorySubmenu').hover(function(){$('#navItemCategorySubmenu').show();$('#navItemCategory').addClass('hover');},
		function(){$('#navItemCategorySubmenu').hide();$('#navItemCategory').removeClass('hover');}
	);

	/* Social buttons
	-----------------------------------*/
	// VK
	VK.init({apiId: 3544299, onlyWidgets: true});
	VK.Widgets.Like("vk_like", {type: "mini", height: 20});
	// Google plus
	window.___gcfg = {lang: 'ru'};
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript';
		po.async = true; po.src = 'https://apis.google.com/js/plusone.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(po, s);
	})();

	/* Home page
	----------------------------------------------------------------------*/
	if ( $('body').hasClass( 'home' ) || $('body').hasClass( 'search' ) || $('body').hasClass( 'product' ) ) {
		setTimeout(function(){
			$('.goodList, #news').equalize();
			$('#news article > a, .goodList .widget footer').css( 'display', 'block' );
		},1000);
	}

	/* Search page
	----------------------------------------------------------------------*/
	if ( $('body').hasClass( 'search' ) ) {
		$('#sidebar .cbColor input').click(function(e){
			var elm = $(this).parent();
			if ( elm.hasClass( 'active' ) )
				elm.removeClass( 'active' );
			else
				elm.addClass( 'active' );
		});
	}

	/* Order page
	----------------------------------------------------------------------*/
	if ( $('body').hasClass( 'order' ) ) {
		var OrderDescr = $('dd');
		OrderDescr.hide().first().show();
		$('dt').click(function(e){
			if ( $(this).hasClass( 'active' ) )
				return false;
			OrderDescr.slideUp(100);
			$('dt.active').removeClass( 'active' );
			$(this).addClass( 'active' ).next( 'dd' ).slideDown( 100 );
		});
	}

	/* Accept page
	----------------------------------------------------------------------*/
	if ( $('body').hasClass( 'accept' ) ) {
		/* Init */
		var tabs = $('.tabs');
		var tabsContent = $('.tabsContent');

		function AcceptChangeTab (elm) {
			if ( $(elm).hasClass( 'active' ) )
				return false;
			$('.active', tabs).removeClass( 'active' );
			var id = $(elm).addClass( 'active' ).attr( 'id' );
			id = id.substr( id.indexOf('-') + 1 );
			window.location.hash = id;
			$('.active', tabsContent).removeClass( 'active' );
			$('#content-' + id).addClass( 'active' );
		}

		/* Change to actual tab */
		if ( window.location.hash.length ) {
			var AcceptHashElm = $('#tab-' + window.location.hash.substr(1));
			if ( AcceptHashElm.length )
				AcceptChangeTab( AcceptHashElm );
		}

		/* Bind actions */
		$('.tab', tabs).click(function(e){
			AcceptChangeTab(this);
		});
	}
});